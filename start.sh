mkdir graphs models results
mkdir graphs/resnet34_unet graphs/unet results/resnet34_unet results/unet models/resnet34_unet models/unet

if [-d pipeline_venv]
then
    echo "pipeline_venv is existing"
else
    python -m venv pipeline_venv
    source pipeline_venv/bin/activate
    pip install --upgrade pip
    pip install poetry
    poetry install
    deactivate
fi
