from layers.conv import conv_block


def unet_block(input_layer, filters, kernel_size, padding, name):
    for i in range(3):
        x = conv_block(
            input_layer=input_layer,
            filters=filters,
            kernel_size=kernel_size,
            padding=padding,
            name=f'{i + 1}-{name}')
    return x
