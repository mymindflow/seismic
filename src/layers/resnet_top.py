from tensorflow.keras.layers import (BatchNormalization, MaxPooling2D,
                                     ZeroPadding2D)

from layers.conv import conv_block


def resnet_top_block(input_layer, filters, padding='same', use_bias=True):
    x = BatchNormalization(name='BN_top', scale=False)(input_layer)
    x = ZeroPadding2D(padding=(3, 3))(x)
    x = conv_block(
        x,
        filters,
        kernel_size=(7, 7),
        strides=(2, 2),
        padding=padding,
        name='top_block',
        kernel_initializer='he_uniform',
        use_bias=use_bias,
        leaky=False)
    x = ZeroPadding2D(padding=(1, 1), name='ZeroPadding_top')(x)
    return MaxPooling2D(
        (3, 3), strides=(2, 2), 
            padding='valid', name='maxPool_top')(x)
