from tensorflow.keras.layers import Conv2DTranspose, UpSampling2D


def up_vs_transpose(x, filters, kernel_size, padding, name, upsampling=True):
    if upsampling:
        return UpSampling2D(name=f'UpSampling_{name}')(x)
    return Conv2DTranspose(
        filters,
        kernel_size,
        strides=(2, 2),
        name=f'TransposeConv_{name}',
        padding=padding)(x)
