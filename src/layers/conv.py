from tensorflow.keras.layers import BatchNormalization, Conv2D, LeakyReLU, ReLU


def conv_block(
        input_layer,
        filters,
        kernel_size=(3,3),
        strides=(1,1),
        padding='same',
        name='block',
        kernel_initializer='glorot_uniform',
        use_bias=True,
        leaky=True):
    x = Conv2D(
        filters=filters,
        kernel_size=kernel_size,
        strides=strides,
        padding=padding,
        name=f'Conv2D_{name}',
        kernel_initializer=kernel_initializer,
        use_bias=use_bias)(input_layer)
    x = BatchNormalization(name=f'BN_{name}')(x)
    if leaky:
        return LeakyReLU(name=f'LeakyReLU_{name}')(x)
    return ReLU(name=f'ReLU_{name}')(x)
