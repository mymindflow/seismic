from tensorflow.keras.layers import (Add, BatchNormalization, Conv2D, ReLU,
                                     ZeroPadding2D)

from layers.conv import conv_block


def resnet_block(
        input_layer,
        filters,
        strides,
        i,
        input=True,
        padding='same',
        use_bias=True):
    x = BatchNormalization(name=f'{i}_BN_1')(input_layer)
    x = ReLU(name=f'{i}_ReLU_1')(x)
    if input:
        y = input_layer
    else:
        y = Conv2D(
            filters,
            (1, 1),
            strides=strides,
            name=f'{i}Conv',
            kernel_initializer='he_uniform',
            padding=padding,
            use_bias=use_bias)(x)
    x = ZeroPadding2D(padding=(1, 1))(x)
    x = conv_block(
        x,
        filters,
        strides=strides,
        padding=padding,
        name=f'{i}conv_block',
        kernel_initializer='he_uniform',
        use_bias=use_bias,
        leaky=False)
    x = ZeroPadding2D(padding=(1, 1))(x)
    x = Conv2D(
        filters,
        (3,
         3),
        name=f'{i}Conv_2',
        kernel_initializer='he_uniform',
        padding=padding,
        use_bias=use_bias)(x)
    return Add()([x, y])
