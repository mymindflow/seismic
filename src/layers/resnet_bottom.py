from tensorflow.keras.layers import (BatchNormalization, Dense,
                                     GlobalAveragePooling2D, ReLU, Softmax)


def resnet_bottom_block(input_layer, classes=1000):
    x = BatchNormalization(name='BN_botton')(input_layer)
    x = ReLU(name='ReLU_bottom')(x)
    x = GlobalAveragePooling2D(name='GlobalPool_bottom')(x)
    x = Dense(classes, name='FC_bottom')(x)
    return Softmax(name='Softmax_bottom')(x)
