from tensorflow.keras.layers import (BatchNormalization, Conv2D, ReLU,
                                     UpSampling2D)


def decode_resnet_unet_block(
        input_layer,
        filters_1,
        filters_2,
        i,
        padding='same',
        up_sampling=True,
        batchnorm=False,
        use_bias=False):
    x = Conv2D(
        filters_1,
        (3, 3),
        name=f'decode_Conv_1_block_{i}',
        kernel_initializer='he_uniform',
        padding=padding,
        use_bias=use_bias)(input_layer)
    if batchnorm:
        x = BatchNormalization(name=f'decode_BN_1_block_{i}', axis=3)(x)
    x = ReLU(name=f'decode_ReLU_1_{i}')(x)
    x = Conv2D(
        filters_2,
        (3, 3),
        name=f'decode_Conv_2_block_{i}',
        kernel_initializer='he_uniform',
        padding=padding,
        use_bias=use_bias)(x)
    if batchnorm:
        x = BatchNormalization(name=f'{i}_BN_2_block_{i}', axis=3)(x)
    x = ReLU(name=f'decode_Relu_2_block_{i}')(x)
    if up_sampling:
        return UpSampling2D(
            size=(2, 2),
            name=f'decode_UpSampling_1_block_{i}')(x)
    return x
