import os

from tensorflow.keras.utils import plot_model

from config import config
from callbacks.callbacks import Callbacks
from config.paths import PLOT_PATH, TRAIN_DATA_PATH
from models.model_map import MODEL_MAP
from train_test.test import report
from utilits.augumentation import train_augmentation, val_augmentation
from utilits.dataset import DataSet
from utilits.labeling import get_labels
from utilits.loader import DataLoader
from utilits.visualization import draw_metrics


def train_model(model, batch_size, epohs):
    model_name = model.model_name
    model = model.build()
    model.compile(config.OPTIMIZER, config.TOTAL_LOSS, config.METRICS)
    plot_model(
        model,
        show_shapes=True,
        to_file=os.path.join(
            PLOT_PATH,
            model_name,
            'graph.png'))

    train_masks, valid_masks, test_masks = get_labels()

    train_dataset = DataSet(
        TRAIN_DATA_PATH,
        train_masks,
        augmentation=train_augmentation())
    valid_dataset = DataSet(
        TRAIN_DATA_PATH,
        valid_masks,
        augmentation=val_augmentation())

    train_dataloader = DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=config.SHUFFLE_DATALOADER)
    valid_dataloader = DataLoader(
        valid_dataset,
        batch_size=batch_size,
        shuffle=config.SHUFFLE_DATALOADER)

    history = model.fit_generator(
        train_dataloader,
        steps_per_epoch=len(train_dataloader),
        epochs=epohs,
        callbacks=Callbacks(model_name, valid_masks).get_callbacks(),
        validation_data=valid_dataloader,
        validation_steps=len(valid_dataloader),
    )

    draw_metrics(history, model_name)
    report(model, test_masks, model_name)
