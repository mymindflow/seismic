import os

import albumentations as A
import numpy as np
import pandas as pd
import tensorflow.keras.backend as K

from config.paths import RESULTS_PATH, TRAIN_DATA_PATH
from losses.dice import dice_coef
from losses.jaccard import jaccard_coef
from utilits.mask_utlits import read_img_mask
from utilits.visualization import draw_errors


def report(model, test_masks, model_name):
    sum_error_jaccard = 0
    sum_error_dice = 0
    n = len(test_masks.ImageId.unique())
    errors = pd.DataFrame(columns=['image', 'jaccard', 'dice'])
    for name_img in test_masks.ImageId.unique():
        img, mask = read_img_mask(name_img, TRAIN_DATA_PATH, test_masks)

        aug = A.Compose([A.Normalize(mean=(0.485, 0.456, 0.406), std=(
            0.229, 0.224, 0.225))])(image=img, mask=mask)
        img, mask = aug['image'], aug['mask']

        img = np.expand_dims(img, axis=0)
        pred = (model.predict(img)).astype('float32')
        mask = mask.astype('float32')
        error_jaccard = K.eval(jaccard_coef(mask, pred[0]))
        error_dice = K.eval(dice_coef(mask, pred[0]))
        errors = errors.append({
            'image': name_img,
            'jaccard': error_jaccard,
            'dice': error_dice,
        }, ignore_index=True)

    sum_error_jaccard += error_jaccard
    sum_error_dice += error_dice

    print(f'коэффициент jaccard на тестовой выборке : {sum_error_jaccard / n}')
    print(f'коэффициент dice на тестовой выборке : {sum_error_dice / n}')

    errors.sort_values(by='dice', inplace=True)
    errors.to_csv(
        os.path.join(
            RESULTS_PATH,
            f'{model_name}_errors.csv'),
        index=False)
    for img_name, title, in zip(
            errors['image'].iloc[[0, -1]], ('worst predict', 'best predict')):
        draw_errors(model, img_name, title, test_masks, model_name)
