import numpy as np
from tensorflow.keras.callbacks import Callback

from utilits.visualization import draw_errors


class PerfomanceCallback(Callback):
    def __init__(self, model_name, valid_mask, freq=1):
        self.model_name = model_name
        self.valid_mask = valid_mask
        self.freq = freq
    
    def on_epoch_end(self, epoch, logs=None):
        if not epoch % self.freq:
            img_name = np.random.choice(self.valid_mask.ImageId.unique(), size=1)[0]
            draw_errors(self.model, img_name, f'on {epoch} for image {img_name}', self.valid_mask, self.model_name)
