from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau

from callbacks.perfomance_callback import PerfomanceCallback
from callbacks.model_checkpoint import CustomModelCheckpoint


class Callbacks:
    def __init__(self, model_name, valid_mask):
        self.model_name = model_name
        self.valid_mask = valid_mask

    def get_callbacks(self):
        return [
            EarlyStopping(
                patience=10,
                verbose=1),
            ReduceLROnPlateau(
                factor=0.1,
                patience=4,
                min_lr=0.00001,
                verbose=1),
            CustomModelCheckpoint(
                model_name=self.model_name,
                monitor='val_dice',
                verbose=1,
                save_best_only=True,
                mode='auto',
                period=20),
            PerfomanceCallback(
                model_name=self.model_name,
                valid_mask=self.valid_mask,
                freq=20
            ),
                ]
