import os

from tensorflow.keras.callbacks import ModelCheckpoint

from config.paths import MODEL_PATH


class CustomModelCheckpoint(ModelCheckpoint):
    def __init__(self, model_name, **kwargs):
        filepath = os.path.join(MODEL_PATH, model_name, 'weights-improvement-{epoch:02d}-{val_dice:.2f}.hdf5')
        super().__init__(filepath=filepath, **kwargs)
