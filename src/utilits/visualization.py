import os

import albumentations as A
import matplotlib.pyplot as plt
import numpy as np

from config.paths import PLOT_PATH, RESULTS_PATH, TRAIN_DATA_PATH
from utilits.mask_utlits import read_img_mask


def template_plot(history, string, model_name):
    plt.figure(figsize=(10, 8))
    plt.plot(history.history[string])
    plt.plot(history.history[f'val_{string}'])
    plt.title(string)
    plt.ylabel(string)
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Valid'], loc='upper rigth')
    plt.savefig(os.path.join(PLOT_PATH, model_name, f'{string}.png'))


def draw_metrics(history, model_name):
    for plot in ('dice', 'loss',):
        template_plot(history, plot, model_name)


def draw_errors(model, img_name, title, test_masks, model_name):
    img, mask = read_img_mask(img_name, TRAIN_DATA_PATH, test_masks)
    aug = A.Compose([A.Normalize(mean=(0.485, 0.456, 0.406),
                    std=(0.229, 0.224, 0.225))])(image=img, mask=mask)
    img, mask = aug['image'], aug['mask']

    _, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, figsize=(8, 8))
    plt.suptitle(
        f'{title}: {img_name} image size:{img.shape} mask size: {mask.shape}')

    ax1.imshow(img)
    ax1.set_title('original image')

    ax2.imshow(np.argmax(mask, axis=-1).astype(np.int32))
    ax2.set_title('original mask')

    img = np.expand_dims(img, axis=0)
    pred = model.predict(img)
    plt.imshow(np.argmax(pred, axis=-1)[0])
    ax3.imshow(np.argmax(pred, axis=-1)[0])
    ax3.set_title('predicted mask')

    error = (np.argmax(mask, axis=-1) != np.argmax(pred, axis=-1))
    ax4.imshow(error[0] > 0)
    ax4.set_title('errors')

    ax6.imshow(img[0])
    ax6.imshow(np.argmax(pred, axis=-1)[0], alpha=0.4)
    ax6.set_title('original mask in image')

    ax5.imshow(img[0])
    ax5.imshow(np.argmax(mask, axis=-1), alpha=0.4)
    ax5.set_title('predicted mask in image')

    plt.savefig(os.path.join(RESULTS_PATH, model_name, f'{title}.png'))
