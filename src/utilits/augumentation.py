import albumentations as A


def train_augmentation():
    train_transform = [
        A.Resize(384, 384),
        A.HorizontalFlip(p=0.5),
        A.Rotate(limit=20, p=0.5),
        A.ElasticTransform(p=0.5),
        A.Normalize(
            mean=(0.485, 0.456, 0.406),
            std=(0.229, 0.224, 0.225)),
    ]
    return A.Compose(train_transform)


def val_augmentation():
    test_transform = [
        A.Resize(384, 384),
        A.Normalize(
            mean=(0.485, 0.456, 0.406),
            std=(0.229, 0.224, 0.225)),
    ]
    return A.Compose(test_transform)
