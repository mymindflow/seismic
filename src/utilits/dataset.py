from utilits.mask_utlits import read_img_mask


class DataSet:
    def __init__(self, train_path, train, augmentation=None):
        self.train_path = train_path
        self.train = train
        self.augmentation = augmentation
        self.lines = train.ImageId.unique()

    def __getitem__(self, idx):
        name_img = self.lines[idx]
        img, mask = read_img_mask(name_img, self.train_path, self.train)
        if self.augmentation:
            aug = self.augmentation(image=img, mask=mask)
            image, mask = aug['image'], aug['mask']
        return image, mask

    def __len__(self):
        return len(self.lines)
