import os

import numpy as np
import pandas as pd

from config.paths import MASK_DATAFRAME_PATH


def get_labels():
    mask = pd.read_csv(MASK_DATAFRAME_PATH)
    valid_idx = np.random.choice(mask.ImageId.unique(), size=int(
        0.25 * len(mask.ImageId.unique())), replace=False)
    valid_masks = mask[mask.ImageId.isin(valid_idx)]
    train = mask[~mask.ImageId.isin(valid_idx)]
    test_idx = np.random.choice(train.ImageId.unique(), size=int(
        0.25 * len(train.ImageId.unique())), replace=False)
    test_masks = train[train.ImageId.isin(test_idx)]
    train_masks = train[~train.ImageId.isin(test_idx)]
    print(f"""size of image set: {len(mask.ImageId.unique())}\n
              size of train set: {len(train_masks.ImageId.unique())}\n
              size of valid set: {len(valid_masks.ImageId.unique())}\n
              size of test set: {len(test_masks.ImageId.unique())}""")

    return train_masks, valid_masks, test_masks
