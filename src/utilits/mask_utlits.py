import os

import cv2
import numpy as np


def rle2mask(mask_rle, shape):
    if mask_rle != mask_rle:
        return np.zeros_like(shape)

    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int)
                       for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0] * shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo: hi] = 1
    return img.reshape(shape).T


def read_img_mask(name_img, train_path, train, one_chanell=False):
    if one_chanell:
        img = np.expand_dims(
            cv2.imread(
                os.path.join(
                    train_path,
                    name_img),
                cv2.IMREAD_GRAYSCALE),
            axis=2)
    else:
        img = cv2.imread(os.path.join(train_path, name_img))
    masks = [rle2mask(rle, shape=(img.shape[1], img.shape[0])) for i, rle in enumerate(
        train[train['ImageId'] == name_img]['EncodedPixels'])]
    mask = np.stack(masks, axis=-1).astype('float')
    if mask.shape[-1] != 1:
        background = 1 - mask.sum(axis=-1, keepdims=True)
        mask = np.concatenate((mask, background), axis=-1)
    return img, mask
