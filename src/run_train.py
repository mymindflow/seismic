import argparse
import os
import random
import warnings

import numpy as np

from config.config import SEED
from models.model_map import MODEL_MAP
from train_test.train import train_model

warnings.filterwarnings('ignore')

os.environ['PYTHONHASHSEED'] = str(SEED)
random.seed(SEED)
np.random.seed(SEED)


def parse_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model',
        '-m',
        help='train model',
        choices=(
            'unet',
            'resnet34_unet',
        ))
    parser.add_argument(
        '--batch',
        '-b',
        help='batch size',
        default=4,
        type=int,
    )
    parser.add_argument(
        '--epohs',
        '-e',
        help='epohs',
        default=200,
        type=int,
    )
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_argument()
    model = train_model(MODEL_MAP[args.model], args.batch, args.epohs)
