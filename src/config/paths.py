
WEIGHT_PATH = '../../resnet34_imagenet_1000.h5'
MODEL_PATH = '../models'

TRAIN_DATA_PATH = '../../images'

MASK_DATAFRAME_PATH = '../../seismic_challenge_train(masks).csv'
TEST_DATAFRAME_PATH = '../../seismic_challenge_sub_baseline_fin.csv'

PLOT_PATH = '../graphs'

RESULTS_PATH = '../results'
