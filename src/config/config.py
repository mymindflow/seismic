from tensorflow.keras.optimizers import Adam

from losses.dice import dice, dice_loss


NUM_CLASSES = 8

SHUFFLE_DATALOADER = True

SEED = 42

USE_PRETRAIN_WEIGHTS = True

TOTAL_LOSS = dice_loss
OPTIMIZER = Adam(learning_rate=0.0001)
METRICS = [dice]
