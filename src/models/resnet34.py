from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model

from layers.resnet_block import resnet_block
from layers.resnet_bottom import resnet_bottom_block
from layers.resnet_top import resnet_top_block


def build_resnet34_classification(
        input_shape,
        weight_path,
        use_pretrain_weights=False,
        include_top=True):
    input_layer = Input(shape=input_shape, name='input')
    x = resnet_top_block(
        input_layer,
        filters=64,
        padding='valid',
        use_bias=False)
    for stage, (i, j) in enumerate(zip([2, 3, 5, 2], [64, 128, 256, 512])):
        if stage == 0:
            strides = (1, 1)
        else:
            strides = (2, 2)
        x = resnet_block(
            input_layer=x,
            filters=j,
            strides=strides,
            input=False,
            i=str(
                stage + 1),
            padding='valid',
            use_bias=False)
        for ind in range(i):
            x = resnet_block(input_layer=x, filters=j, strides=(1, 1), input=True, i=str(
                ind) + '_' + str(stage + 1), padding='valid', use_bias=False)
    x = resnet_bottom_block(x)
    model = Model(input_layer, x)
    if use_pretrain_weights:
        print('imagenet weights was load')
        model.load_weights(weight_path)
    if include_top:
        return model
    else:
        return Model(model.input, model.get_layer('ReLU_bottom').output)
