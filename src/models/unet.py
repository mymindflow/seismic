from tensorflow.keras.layers import Conv2D, Input, MaxPool2D, concatenate
from tensorflow.keras.models import Model

from config.config import NUM_CLASSES
from layers.unet_block import unet_block
from layers.upsampling import up_vs_transpose


class Unet:
    model_name = 'unet'

    def __init__(self, input_shape, num_classes=NUM_CLASSES, upsampling=False):
        self.input_shape = input_shape
        self.num_classes = num_classes
        self.upsampling = upsampling

    def build(self):
        input = Input(shape=self.input_shape, name='input')
        output_layes = []
        x = unet_block(input, 64, (3, 3), 'same', f'decode_0')
        y = MaxPool2D(name='MP_0')(x)
        output_layes.append(y)
        for i, filters in enumerate([128, 192, 256]):
            x = unet_block(y, filters, (3, 3), 'same', f'decode_{i + 1}')
            y = MaxPool2D(name=f'MP_{i + 1}')(x)
            output_layes.append(y)

        x = unet_block(y, 256, (3, 3), 'same', 'middle')
        x = concatenate([x, y], axis=-1)

        for i, filters in enumerate([256, 192, 128]):
            x = unet_block(x, filters, (3, 3), 'same', f'encode_{i + 1}')
            x = up_vs_transpose(
                x, filters, (2, 2), 'same', str(
                    i + 1), self.upsampling)
            x = concatenate([x, output_layes[-2 - i]], axis=-1)

        x = unet_block(x, 64, (3, 3), 'same', 'encode_4')
        x = up_vs_transpose(x, 64, (2, 2), 'same', '4', self.upsampling)
        output = Conv2D(
            filters=self.num_classes,
            kernel_size=1,
            activation='sigmoid',
            name='output')(x)
        return Model(input, output)
