from models.resnet34_unet import Resnet34Unet
from models.unet import Unet

MODEL_MAP = {
    'resnet34_unet': Resnet34Unet(input_shape=(None, None, 3)),
    'unet': Unet(input_shape=(None, None, 3)),
}
