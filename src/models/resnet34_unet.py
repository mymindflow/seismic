from tensorflow.keras.layers import Conv2D, UpSampling2D, concatenate
from tensorflow.keras.models import Model

from config.config import NUM_CLASSES, USE_PRETRAIN_WEIGHTS
from config.paths import WEIGHT_PATH
from layers.resnet_unet_decode import decode_resnet_unet_block
from models.resnet34 import build_resnet34_classification


class Resnet34Unet:
    model_name = 'resnet34_unet'

    def __init__(
            self,
            input_shape,
            weight_path=WEIGHT_PATH,
            use_pretrain_weights=USE_PRETRAIN_WEIGHTS,
            num_classes=NUM_CLASSES):
        self.input_shape = input_shape
        self.weight_path = weight_path
        self.use_pretrain_weights = use_pretrain_weights
        self.num_classes = num_classes

    def build(self):
        resnet34 = build_resnet34_classification(
            input_shape=self.input_shape,
            weight_path=self.weight_path,
            use_pretrain_weights=self.use_pretrain_weights,
            include_top=False)
        x1 = resnet34.get_layer('2_ReLU_1').output
        x2 = resnet34.get_layer('3_ReLU_1').output
        x3 = resnet34.get_layer('4_ReLU_1').output
        x = concatenate(
            [UpSampling2D(size=(2, 2))(resnet34.output), x3], axis=-1)
        x = decode_resnet_unet_block(x, 256, 256, i='1')
        x = concatenate([x, x2], axis=-1)
        x = decode_resnet_unet_block(x, 128, 128, i='2', batchnorm=True)
        x = concatenate([x, x1], axis=-1)
        x = decode_resnet_unet_block(x, 64, 64, i='3', batchnorm=True)
        x = decode_resnet_unet_block(x, 32, 32, i='4', batchnorm=True)
        x = decode_resnet_unet_block(
            x, 16, 16, i='5', up_sampling=False, batchnorm=True)
        x = Conv2D(
            self.num_classes,
            (1,
             1),
            name='prediction',
            kernel_initializer='glorot_uniform',
            use_bias=True,
            padding='same',
            activation='sigmoid')(x)
        return Model(resnet34.input, x)
