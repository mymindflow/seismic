from tensorflow import reduce_sum
from tensorflow.keras.backend import flatten
from tensorflow.keras.losses import binary_crossentropy


def dice_coef(y_true, y_pred, smoothing=1.):
    y_true_f = flatten(y_true)
    y_pred_f = flatten(y_pred)
    intersection = reduce_sum(y_true_f * y_pred_f)
    return ((2. * intersection + smoothing) /
            (reduce_sum(y_true_f) + reduce_sum(y_pred_f) + smoothing))


def dice(y_true, y_pred):
    return 1 - dice_coef(y_true, y_pred)


def dice_loss(y_true, y_pred):
    return 0.75 * dice(y_true, y_pred) + 0.25 * \
        binary_crossentropy(y_true, y_pred)
