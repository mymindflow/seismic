from tensorflow import reduce_mean, reduce_sum
from tensorflow.keras.losses import binary_crossentropy


def jaccard_coef(y_true, y_pred, smoothing=1.):
    intersection = reduce_sum(y_true * y_pred, axis=(1, 2))
    union = reduce_sum(y_true + y_pred, axis=(1, 2))
    jaccard = (intersection + smoothing) / (union - intersection + smoothing)
    return reduce_mean(jaccard)


def jaccard(y_true, y_pred):
    return 1. - jaccard_coef(y_true, y_pred)


def jaccard_loss(y_true, y_pred):
    return 0.75 * jaccard(y_true, y_pred) + 0.25 * \
        binary_crossentropy(y_true, y_pred)
