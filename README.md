# Сегментация сейсмических отражающих горизонтов
## Задача
- Задача: сегментация сейсмических горизонтов в кубе амплитуд мелового и юрского периодов.


- Количество классов: 7

- Количество изображений: 896
  - xline:  512
  - inline:  384
  
- Размеры изображений:
  - xline: 384x512
  - inline:  384х384

## Полезные ссылкы
посты на эту тему:[habr.com](https://habr.com/ru/company/ods/blog/482780/),
[habr.com](https://habr.com/ru/company/ods/blog/488852/)


близкие соревнования:[kaggle.com](https://www.kaggle.com/c/tgs-salt-identification-challenge)


## Данные

исходные данные данные: [booster.ru](https://boosters.pro/championship/rsc_sandbox/overview)



содежимое исходных данных:
+ набор изображений для обучения
+ набор изображений для тестирования
+ baseline на pytorch с ResNet34
+ маски(.csv файл)
+ пример сабмита
+ .npy файлы
+ функции для извлечения изображений из .npy файлов
